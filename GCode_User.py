# -*- encoding: utf-8 -*-
"""
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Tento plugin navazuje na plugin GCode_small.
Jeho ucel je co nejvic zmensit objem prenasenych dat po seriove lince nebo SD.
To zabrani nechtenym pauzam pri tisku a nezaplnovani bufferu.
Take snizi delku vygenerovaneho GCodu na minimum.

Funkce:
*   Odstranuje duplicitni souradnice X, Y, Z, F
*   Odstranuje Marlinem nepouzivane MCody (Volitelny)
*   Podporuje Marlin FW Retract / G10, G11 / (Volitelny)
*   Vytvari tagy pro muj GCodeViewer

INSTALL:
Copy this file into 'skeinforge_application/skeinforge_plugins/craft_plugins/export_plugins/static_plugins'
and in SkeinLAB gui or Skeinforge select Export plugin Gcode USER.
that's all.
"""

from __future__ import absolute_import
import cStringIO
import os

# use marlin firmware retract ? need to enable it in FW !!!
MARLIN_FW_RETRACT = True

# filter unused mcodes for marlin ?
FILTER_UNUSED_MARLIN_CODE = True

__author__ = 'Martin Bílek (martblek@gmail.com)'
__version__ = "0.02"
__date__ = '$Date: 2015/08/02 $'
__license__ = 'GNU AGPL http://www.gnu.org/licenses/agpl.html'

# kody nepouzitelne v Marlin FW, (http://reprap.org/wiki/G-code)
UNUSED_MARLIN_M_CODES = ['M2', 'M36', 'M70', 'M72', 'M73', 'M93', 'M98', 'M99',
                        'M101', 'M102', 'M103', 'M108', 'M110', 'M111', 'M113',
                        'M116', 'M118', 'M122', 'M124', 'M130', 'M131', 'M132',
                        'M133', 'M141', 'M142', 'M143', 'M144', 'M146', 'M149',
                        'M191', 'M210', '211']

# This is true if the output is text and false if it is binary."
globalIsReplaceable = True


def getIndexOfStartingWithSecond(letter, splitLine):
    """Get index of the first occurence of the given letter in the split line,
    starting with the second word.  Return - 1 if letter is not found"""

    for wordIndex in xrange(1, len(splitLine)):
        word = splitLine[wordIndex]
        firstLetter = word[0]
        if firstLetter == letter:
            return wordIndex
    return - 1


def getOutput(gcodeText):
    'Get the exported version of a gcode file.'
    return GcodeSWSkein().getCraftedGcode(gcodeText)


def getSplitLineBeforeBracketSemicolon(line):
    "Get the split line before a bracket or semicolon."

    bracketSemicolonIndex = min(line.find(';'), line.find('('))
    if bracketSemicolonIndex < 0:
        return line.split()
    return line[:bracketSemicolonIndex].split()


def getStringFromCharacterSplitLine(character, splitLine):
    """Get the string after the first occurence of the character
    in the split line."""

    indexOfCharacter = getIndexOfStartingWithSecond(character, splitLine)
    if indexOfCharacter < 0:
        return None
    return splitLine[indexOfCharacter][1:]


def getSummarizedFileName(fileName):
    if os.getcwd() == os.path.dirname(fileName):
        return os.path.basename(fileName)
    return fileName


def getTextLines(text):
    "Get the all the lines of text of a text."
    return text.replace('\r', '\n').split('\n')


class GcodeSWSkein:
    "A class to remove redundant z and feed rate parameters from a skein of extrusions."
    def __init__(self):
        self.lastFeedRateString = None
        self.lastXString = None
        self.lastYString = None
        self.lastZString = None
        self.output = cStringIO.StringIO()

    def getCraftedGcode( self, gcodeText ):
        "Parse gcode text and store the gcode."
        lines = getTextLines(gcodeText)
        self.output.write(';GCode Crafter File v0.02\n;generated using SkeinLAB Plugin\n\n')
        self.output.write('M117 Support SkeinLAB\n')
        for line in lines:
            self.parseLine(line)
        return self.output.getvalue()

    def parseLine(self, line):
        "Parse a gcode line."
        splitLine = getSplitLineBeforeBracketSemicolon(line)
        if len(splitLine) < 1:
            return
        firstWord = splitLine[0]
        if len(firstWord) < 1:
            return

        if firstWord[0] == '(':
            if firstWord == '(<layer>':
                self.output.write('\n;Starting_New_Layer\n')
            return

        if firstWord == 'M101':
            if MARLIN_FW_RETRACT:
                # change to unretract G11 for Marlin
                self.output.write('G11\n')
                return

        if firstWord == 'M103':
            if MARLIN_FW_RETRACT:
                # change to FW retract in Marlin
                self.output.write('G10\n')
                return

        if FILTER_UNUSED_MARLIN_CODE:
            """ Filter unused Marlin MCodes """
            if firstWord in UNUSED_MARLIN_M_CODES:
                return

        if firstWord != 'G1':
            self.output.write(line + '\n')
            return

        eString = getStringFromCharacterSplitLine('E', splitLine)
        xString = getStringFromCharacterSplitLine('X', splitLine)
        yString = getStringFromCharacterSplitLine('Y', splitLine)
        zString = getStringFromCharacterSplitLine('Z', splitLine)
        feedRateString = getStringFromCharacterSplitLine('F', splitLine)

        line = 'G1'
        if xString is not None and xString != self.lastXString:
            line = line + ' X%s' % xString

        if yString is not None and yString != self.lastYString:
            line = line + ' Y%s' % yString

        if zString is not None and zString != self.lastZString:
            line = line + ' Z%s' % zString

        if feedRateString is not None and feedRateString != self.lastFeedRateString:
            line = line + ' F%s' % feedRateString

        if eString is not None:
            line = line + ' E%s' % eString

        if len(line) > 2:
            self.output.write(line + '\n')

        self.lastFeedRateString = feedRateString
        self.lastZString = zString
        self.lastXString = xString
        self.lastYString = yString